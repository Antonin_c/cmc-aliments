const slider = document.querySelector(".Slider");
const nextBtn = document.querySelector(".next-btn");
const prevBtn = document.querySelector(".prev-btn");
const slides = document.querySelectorAll(".Slide");
const slideIcons = document.querySelectorAll(".slide-icon");
const numberOfSlides = slides.length;
var slideNumber = 0;

//Image Next-Slide

nextBtn.addEventListener('click', () => {

    slides.forEach((slide) => {
        slide.classList.remove("active");
    });

    slideIcons.forEach((slideIcon) => {
        slideIcon.classList.remove("active");
    });

    slideNumber++;

    if(slideNumber > (numberOfSlides - 1)){
        slideNumber = 0;

    }
  slides[slideNumber].classList.add("active");
  slideIcons[slideNumber].classList.add("active");
});

//Image Prev-Slide

prevBtn.addEventListener('click', () => {

    slides.forEach((slide) => {
        slide.classList.remove("active");
    });

    slideIcons.forEach((slideIcon) => {
        slideIcon.classList.remove("active");
    });

    slideNumber--;

    if(slideNumber < 0){
        slideNumber = numberOfSlides - 1;

    }
  slides[slideNumber].classList.add("active");
  slideIcons[slideNumber].classList.add("active");

});

//Image Slider Auto-Play

var playSlider;
var repeater = () => {
    playSlider = setInterval(function(){

        slides.forEach((slide) => {
            slide.classList.remove("active");
        });
    
        slideIcons.forEach((slideIcon) => {
            slideIcon.classList.remove("active");
        });
    
        slideNumber++;
    
        if(slideNumber > (numberOfSlides - 1)){
            slideNumber = 0;
    
        }
      slides[slideNumber].classList.add("active");
      slideIcons[slideNumber].classList.add("active");


    }, 4000);
}
repeater();


//Stop Auto-Slide on hover 

slider.addEventListener("mouseover", () => {

    clearInterval(playSlider);
});

//Auto-play  on Out

slider.addEventListener("mouseout", () => {
    repeater();
});