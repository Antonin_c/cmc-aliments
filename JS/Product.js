let url_Produits = `https://directus.antonin-c-portfolio.fr/items/Produits`;
var containers = document.getElementById("container");
let count = 1;

var Categorie = 0;


function strip_tag(string) {
    return string.replace(/(<([^>]+)>)/gi, "");
}



async function GetProduct() {
const data = await fetch (url_Produits,{ // contacte L'API
       method: "GET", 
       headers: {
          "Content-Type":"application/json"
       }
    })
    if(data.ok){ // .ok = si la réponse de l'api est 200
     const response = await data.json(); // la fonction await intérompt l'execution du code pour atendre la réponse 
    response["data"].forEach(async element => {
        
    if(Categorie == 0){
        let New_Product = document.createElement("div");
        New_Product.setAttribute('class','Card');

        let New_Content = document.createElement("div");
        New_Content.setAttribute('class','content')

        let New_Name= document.createElement("h4");
        New_Name.setAttribute('class','name');
        New_Name.textContent = element['Nom_Produits'];


        let New_img = document.createElement("img");
        New_img.setAttribute('src', "https://directus.antonin-c-portfolio.fr/assets/" + element["Images_Du_Produits"]);
        New_img.setAttribute('class','Product-image');

        let New_Description = document.createElement("div");
        New_Description.setAttribute('class', 'description');

            let New_P = document.createElement("p");
            New_P.textContent = strip_tag(element['Descriptions']);




        let New_Poids = document.createElement('h4');
        New_Poids.setAttribute('class','Poids');
        New_Poids.textContent = "Poids Net : " + element["Poids_Net"];

        let New_Info = document.createElement("div");
        New_Info.setAttribute('class','Info');

            let New_Prix = document.createElement('h2');
            New_Prix.setAttribute('class','Prix');
            New_Prix.textContent = element['Prix'] + '€';
        

        let New_counter = document.createElement("div");
        New_counter.setAttribute('class','counter');

            let New_counter_span = document.createElement("span");
            New_counter_span.setAttribute('class','down');
            New_counter_span.setAttribute('onClick','decreaseCount(event, this)');
            New_counter_span.textContent = '-';


            let New_default_Value = document.createElement("input");
            New_default_Value.setAttribute('type','text');
            New_default_Value.setAttribute('value','1');
            New_default_Value.setAttribute('id','input'+count)

            let New_counter_span2 = document.createElement("span");
            New_counter_span2.setAttribute('class','up');
            New_counter_span2.setAttribute('onClick','increaseCount(event, this)');
            New_counter_span2.textContent = "+";



        let New_Cart_button = document.createElement("button");
        New_Cart_button.setAttribute('class','Add-Cart');
        New_Cart_button.setAttribute('onClick',"GetNumberOfProduct(this)")
        New_Cart_button.setAttribute('id', count);
        New_Cart_button.textContent = "Ajouter Au Panier";



        New_Description.appendChild(New_P);

        New_counter.appendChild(New_counter_span);
        New_counter.appendChild(New_default_Value);
        New_counter.appendChild(New_counter_span2);

        New_Info.appendChild(New_Prix);
        New_Info.appendChild(New_counter);


        New_Content.appendChild(New_img)
        New_Content.appendChild(New_Name);
        New_Content.appendChild(New_Description);
        New_Content.appendChild(New_Poids);
        New_Content.appendChild(New_Info);
        New_Content.appendChild(New_Cart_button);
        
        
        New_Product.appendChild(New_Content);
        containers.appendChild(New_Product);

        count++;
    }

    else{
        


































    }
    });


    }
    else{
        console.log("ERREUR"); // si il n'y a pas de réponse positive 
    }
}



function navigation(elements){
   let btn = document.getElementById(elements.id);

    btn.addEventListener('click',function(e){
    e.preventDefault();
    
    console.log(btn);

   })
}
