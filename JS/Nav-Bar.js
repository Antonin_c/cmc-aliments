const menuHamburger = document.querySelector(".menu-hamburger");
const navLinks = document.querySelector(".nav-links");
const body = document.querySelector("body"); 



menuHamburger.addEventListener('click',()=>{
    //enable Burger menu
    navLinks.classList.toggle('mobile-menu');
    if (navLinks.classList.contains('mobile-menu')) { 
        // Disable scroll 
        body.style.overflow = "hidden"; 
    } else { 
        // Enable scroll 
        body.style.overflow = "auto"; 
    }
    
})
