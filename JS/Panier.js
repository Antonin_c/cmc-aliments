function SauvegardePanier(Panier){
    localStorage.setItem("Panier", JSON.stringify(Panier));
}

function getPanier(){

    let Panier = localStorage.getItem("Panier");
    if( Panier == null){
        return [];
    }
    else{
        return JSON.parse(Panier);
    }
}

function AjoutProduits(product){
    let Panier =getPanier();
    let foundPanier = Panier.find(p => p.id == product.id)
    if (foundPanier != undefined) {
        foundPanier.quantity++
    }
    else{
        product.quantity = 1;
        Panier.push(product);
    }
    SauvegardePanier(Panier);
}

function SuppPanier(product){
    let Panier = getPanier();
    Panier = Panier.filter(p => p.id != product.id);
    SauvegardePanier(Panier);
}

function ChangerQuantiter(product,quantity){
    let Panier = getPanier();
    let foundProduit = Panier.find(p => p.id == product.id);
    if (foundProduit != undefined) {
        foundProduit.quantity += quantity;
        if(foundProduit.quantity <= 0) {
            SuppPanier(foundProduit);
        } else {

            SauvegardePanier(Panier);
        }
    } 
}

function TotalProduits(){
    let Panier = getPanier();
    let number = 0;
    for(let product of Panier){
        number += product.quantity;
    }
    return number;
}

function TotalPrix(){

    let Panier = getPanier();
    let total = 0;
    for(let product of Panier){
        total += product.quantity * product.Prix;
    }
    return total;
}